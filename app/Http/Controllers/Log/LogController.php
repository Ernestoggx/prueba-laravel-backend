<?php

namespace App\Http\Controllers\Log;

use App\Http\Controllers\ApiController;
use App\Models\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class LogController extends ApiController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required',
            'priority' => 'required',
            'path' => 'required',
            'message' => 'required',
            'request' => 'required',
            'response' => 'required',
            'aplication_id' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json(['status'=>'error','errors'=>$validator->errors()], 401);
        }
        $tempArray = json_decode($request->getContent(), true);
        $log = Log::create($tempArray);
        return $this->showOne($log, 201); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try{
            if($id !== ''){
                $log = Log::where('aplication_id',$id)->first();
                if($log != null){
                    $log = Log::where('aplication_id',$id)->get();
                    return response()->json(['data' => $log, 'code' => 200]);
                }else{
                    return $this->errorResponse('No existe el recurso', 404);
            }
            }else{
                return response()->json(['code' => 404]);
            }
        } catch (Throwable $e){
            return $this->errorResponse('Ha ocurrido un error, por favor intente de nuevo', 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
