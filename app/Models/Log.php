<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $fillable = [
        'type',
        'priority',
        'path',
        'message',
        'request',
        'response',
        'aplication_id'
    ];

    public function aplication(){
        return $this->belongsTo(Aplication::class);
    }

}
