<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Aplication extends Model
{
    protected $fillable = [
        'name'
    ];

    public function log(){
        return $this->hasMany(Log::class);
    }

}
