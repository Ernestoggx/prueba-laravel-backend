<?php
  namespace App\Traits;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

trait ApiResponser{

    private function successResponse($data, $code){
      return response()->json($data, $code);
    }

    protected function errorResponse($message, $code){
      return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, $code= 200){
      $collection = $this->sortData($collection);
      return response()->json(['data' => $collection, 'code'=> $code], $code);
    }

    protected function showOne(Model $instance, $code= 200){
      return response()->json(['data' => $instance, 'code'=> $code], $code);
    }

    protected function sortData(Collection $collection){
      if (request()->has('sort_by')){
        $attribute = request()->sort_by;
        $collection = $collection->sortBy->{$attribute};
      }
      return $collection;
    }

  }
