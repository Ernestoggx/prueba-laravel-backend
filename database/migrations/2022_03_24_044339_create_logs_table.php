<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type', 100);
            $table->string('priority', 100);
            $table->mediumText('path');
            $table->string('message', 300);
            $table->string('request', 300);
            $table->string('response', 300);
            $table->integer('aplication_id')->unsigned();
            $table->timestamps();

            $table->foreign('aplication_id')->references('id')->on('aplications');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('logs');
    }
}
